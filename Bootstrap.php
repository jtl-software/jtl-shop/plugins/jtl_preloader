<?php declare(strict_types=1);

namespace Plugin\jtl_preloader;

use Illuminate\Support\Collection;
use JTL\Events\Dispatcher;
use JTL\Helpers\Form;
use JTL\Plugin\Bootstrapper;
use JTL\Plugin\Data\Config;
use JTL\Session\Storage;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use stdClass;

/**
 * Class Bootstrap
 * @package Plugin\jtl_preloader
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritdoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        if (isset($_SESSION['oBesucher']->kBesucher) || Shop::isFrontend() === false) {
            return;
        }
        $config = $this->getPlugin()->getConfig();
        $isBot  = $config->getValue('disable_for_bots') === 'Y'
            ? Storage::getIsCrawler($_SERVER['HTTP_USER_AGENT'] ?? '')
            : false;
        if ($isBot === false || $config->getValue('disable_for_bots') === 'N') {
            $this->setHeader($config);
        }
    }

    /**
     * @param Config $config
     */
    private function setHeader(Config $config): void
    {
        $header = null;
        $links  = [];
        foreach (\array_filter(\explode(';', $config->getValue('preload'))) as $resource) {
            [$src, $type] = \explode('#', $resource);
            $links[]      = '<' . $src . '>;rel=preload;as=' . $type . ($type === 'font' ? ';crossorigin' : '');
        }
        foreach (\array_filter(\explode(';', $config->getValue('prefetch'))) as $src) {
            $links[] = '<' . $src . '>;rel=prefetch';
        }
        foreach (\array_filter(\explode(';', $config->getValue('dns_prefetch'))) as $src) {
            $links[] = '<' . $src . '>;rel=dns-prefetch';
        }
        if (\count($links) > 0) {
            $header = 'Link:' . \implode(',', $links);
        }
        if ($header !== null) {
            \header($header, false);
        }
    }

    /**
     * @inheritdoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        if ($tabName !== 'Ressourcen') {
            return '';
        }
        $pluginConf = $this->getPlugin()->getConfig();
        if (!empty($_POST['update-urls']) && Form::validateToken()) {
            $values = $pluginConf->getOptions();
            $value  = '';
            if (!empty($_POST['preload-url'])) {
                foreach ($_POST['preload-url'] as $idx => $data) {
                    $value .= $data . '#' . $_POST['preload-type'][$idx] . ';';
                }
                $value = \rtrim($value, ';');
            }
            $this->updatePluginOption($values, 'preload', $value);

            $value = \implode(';', $_POST['prefetch-url'] ?? []);
            $this->updatePluginOption($values, 'prefetch', $value);

            $value = \implode(';', $_POST['dns-prefetch-url'] ?? []);
            $this->updatePluginOption($values, 'dns_prefetch', $value);
            $this->getCache()->flushTags([\CACHING_GROUP_PLUGIN]);
        }
        $preload     = [];
        $prefetch    = [];
        $dnsPrefetch = [];
        foreach (\array_filter(\explode(';', $pluginConf->getValue('preload'))) as $opt) {
            $data                     = new stdClass();
            [$data->src, $data->type] = \explode('#', $opt);
            $preload[]                = $data;
        }
        foreach (\array_filter(\explode(';', $pluginConf->getValue('prefetch'))) as $opt) {
            $prefetch[] = $opt;
        }
        foreach (\array_filter(\explode(';', $pluginConf->getValue('dns_prefetch'))) as $opt) {
            $dnsPrefetch[] = $opt;
        }

        return $smarty->assign('types', ['script', 'style', 'image', 'font', 'audio', 'video'])
            ->assign('confPreload', $preload)
            ->assign('confPrefetch', $prefetch)
            ->assign('confDNSPrefetch', $dnsPrefetch)
            ->fetch($this->getPlugin()->getPaths()->getAdminPath() . 'urls.tpl');
    }

    /**
     * @param Collection $options
     * @param string     $name
     * @param string     $value
     */
    private function updatePluginOption(Collection $options, string $name, string $value): void
    {
        $hit = $options->first(static function ($e) use ($name) {
            return $e->valueID === $name;
        });
        if ($hit !== null) {
            $hit->value = $value;
            $this->getDB()->update(
                'tplugineinstellungen',
                ['kPlugin', 'cName'],
                [$this->getPlugin()->getID(), $name],
                (object)['cWert' => $value]
            );
        }
        $this->getPlugin()->getConfig()->setOptions($options);
    }
}
