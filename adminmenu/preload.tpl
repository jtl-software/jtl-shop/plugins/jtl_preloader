<div class="form-group form-row align-items-center col-xs-12 col-md-12"">
    <div class="col col-sm-5 input-group">
        <div class="input-group-prepend">
            <label class="input-group-text">{__('URL')}</label>
        </div>
        <input type="text" name="preload-url[]" class="form-control" placeholder="{__('URL')}" value="{if $c !== null}{$c->src}{/if}" aria-label="{__('URL')}">
    </div>
    <div class="input-group col col-sm-3">
        <div class="input-group-prepend">
            <label class="input-group-text">{__('Type')}</label>
        </div>
        <select class="custom-select form-control" name="preload-type[]">
            {foreach $types as $type}
                <option value="{$type}"{if $c != null && $c->type === $type} selected{/if}>{$type}</option>
            {/foreach}
        </select>
        <div class="input-group-append">
            <button type="button" class="btn btn-danger delete-row" aria-label="{__('delete')}"><i class="fa fa-trash"></i></button>
        </div>
    </div>
</div>
