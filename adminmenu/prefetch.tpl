<div class="form-group col-xs-12 col-md-12">
    <div class="input-group form-row align-items-center col col-sm-5">
        <div class="input-group-prepend">
            <label class="input-group-text">{__('URL')}</label>
        </div>
        <input type="text" name="prefetch-url[]" class="form-control" placeholder="{__('URL')}"{if $c !== null} value="{$c}"{/if}>
        <div class="input-group-append">
            <button type="button" class="btn btn-danger delete-row" aria-label="{__('delete')}"><i class="fa fa-trash"></i></button>
        </div>
    </div>
</div>
