<form method="post" class="form">
    {$jtl_token}
    <input type="hidden" name="update-urls" value="1">
    <div class="card">
        <div class="card-header">
            <div class="subheading1">{__('Resources to preload via re=preload')}</div>
            <hr class="mb-n3">
        </div>
        <div class="card-body">
            {foreach $confPreload as $c}
                {include file='./preload.tpl'}
            {/foreach}
            <div id="add-preload-zone" class="hidden"></div>
        </div>
        <div class="card-footer">
            <button type="button" class="btn btn-default add-row" id="add-preload">
                <i class="fa fa-share"></i> {__('add new')}
            </button>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="subheading1">{__('Resources to preload via re=prefetch')}</div>
            <hr class="mb-n3">
        </div>
        <div class="card-body">
            {foreach $confPrefetch as $c}
                {include file='./prefetch.tpl'}
            {/foreach}
            <div id="add-prefetch-zone" class="hidden"></div>
        </div>
        <div class="card-footer">
            <button type="button" class="btn btn-default add-row" id="add-prefetch">
                <i class="fa fa-share"></i> {__('add new')}
            </button>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="subheading1">{__('Domains to preload via rel=dns-prefetch')}</div>
            <hr class="mb-n3">
        </div>
        <div class="card-body">
            {foreach $confDNSPrefetch as $c}
                {include file='./prefetchDNS.tpl'}
            {/foreach}
            <div id="add-dns-prefetch-zone" class="hidden"></div>
        </div>
        <div class="card-footer">
            <button type="button" class="btn btn-default add-row" id="add-dns-prefetch">
                <i class="fa fa-share"></i> {__('add new')}
            </button>
        </div>
    </div>

    <button class="btn btn-primary" type="submit">
        <i class="fa fa-save"></i> {__('save')}
    </button>
</form>

{capture name="preloadInput"}
    {include file='./preload.tpl' c=null}
{/capture}

{capture name="prefetchInput"}
    {include file='./prefetch.tpl' c=null}
{/capture}

{capture name="prefetchDNSInput"}
    {include file='./prefetchDNS.tpl' c=null}
{/capture}

<script type="text/javascript">
    var preloadInput = '{$smarty.capture.preloadInput|strip}',
        prefetchInput = '{$smarty.capture.prefetchInput|strip}',
        prefetchDNSInput = '{$smarty.capture.prefetchDNSInput|strip}';

    $('#add-preload').on('click', function () {
        $(preloadInput).insertBefore('#add-preload-zone');
        return false;
    });
    $('#add-prefetch').on('click', function () {
        $(prefetchInput).insertBefore('#add-prefetch-zone');
        return false;
    });
    $('#add-dns-prefetch').on('click', function () {
        $(prefetchDNSInput).insertBefore('#add-dns-prefetch-zone');
        return false;
    });
    $('.settings').on('click', '.delete-row', function () {
        this.closest('.form-group').remove();
    });
</script>
